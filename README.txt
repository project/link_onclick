# Link: Onclick Attribute

## Description

Adds an "onclick" attribute to link fields. The attribute is configurable on a per field basis (from the field widget settings form).

## Usage

- Download and install the module as usual [@see Installing Modules & Themes](https://www.drupal.org/documentation/install/modules-themes)
- Enable the onclick attribute by editing a link field from the "Manage Fields" page of your content type (or any entity bundle type for that matter).
- Visit the add/edit page for your content type and behold your onclick attribute field!

## Warning

Giving users access to run any javascript they want from the onclick attribute may not be the most secure thing in some cases. Use sparingly and with caution!

## Dependencies

- [Link](https://www.drupal.org/project/link) module (version 7.x-1.3 or higher).
