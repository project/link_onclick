<?php

/**
 * @file
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function link_onclick_form_field_ui_field_edit_form_alter(&$form, &$form_state) {

  if ($form['#field']['type'] == 'link_field') {

    // Grab the instance settings
    $settings = $form['#instance']['settings'];

    $form['instance']['settings']['onclick'] = array(
      '#type' => 'fieldset',
      '#title' => t('Onclick Attribute'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    $form['instance']['settings']['onclick']['enable'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable "onclick" link attribute.'),
      '#description' => t('Check this box to enable a field for the "onclick" attibute of the link field. Can be useful for adding click event tracking for google analytics. <em><strong>Be careful with this module, however, as it technically accepts any javascript snippet the user provides and could potentially be a security issue in the wrong hands!.</strong></em>'),
      '#default_value' => isset($settings['onclick']['enable'])
          ? $settings['onclick']['enable'] : 0,
    );
  }

}

/**
 * Implements hook_element_info_alter().
 *
 * Adds the onclick attribute setting to link fields.
 */
function link_onclick_element_info_alter(&$types) {
  // Append a process function for the field integration.
  if (isset($types['link_field'])) {
    $types['link_field']['#process'][] = 'link_onclick_process_widget';
  }
}

/**
 * Process callback.
 */
function link_onclick_process_widget($element) {
  if (!isset($element['#entity_type'])) {
    return $element;
  }

  $field = field_info_field($element['#field_name']);
  $instance = field_info_instance($element['#entity_type'], $element['#field_name'], $element['#bundle']);

  if (!empty($instance['settings']['onclick']['enable'])) {
    $element['attributes']['onclick'] = array(
      '#type' => 'textfield',
      '#title' => t('Onclick'),
      '#description' => t('Add an onclick event to this link. Useful for adding analytics tracking to link clicks.'),
      '#default_value' => isset($element['#default_value']['attributes']['onclick']) ? $element['#default_value']['attributes']['onclick'] : '',
      '#prefix' => '<div class="link-attributes">',
      '#suffix' => '</div>',
    );
  }

  return $element;
}
